package com.muhardin.training.iso8583.server;

import java.util.HashMap;
import java.util.Map;

public class MessageConstants {
    public static final Map<Integer, Integer> DATA_ELEMENT_LENGTH
            = new HashMap<>();

    static {
        DATA_ELEMENT_LENGTH.put(2, 18);
        DATA_ELEMENT_LENGTH.put(3, 6);
        DATA_ELEMENT_LENGTH.put(11, 6);
        DATA_ELEMENT_LENGTH.put(12, 6);
        DATA_ELEMENT_LENGTH.put(13, 8);
        DATA_ELEMENT_LENGTH.put(32, 10);
        DATA_ELEMENT_LENGTH.put(37, 12);
        DATA_ELEMENT_LENGTH.put(41, 8);
        DATA_ELEMENT_LENGTH.put(49, 4);
        DATA_ELEMENT_LENGTH.put(52, 4);
        DATA_ELEMENT_LENGTH.put(70, 3);
    }
}
