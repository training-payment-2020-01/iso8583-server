package com.muhardin.training.iso8583.server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ServerApplication {
    public static void main(String[] args) throws IOException {
        // 1. Start socket server
        Integer port = 12345;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Starting app at port "+port);
        Socket socket = serverSocket.accept();

        // 2. Create input stream reader for incoming data
        InputStreamReader reader = new InputStreamReader(socket.getInputStream());

        // 3. Read message length
        char[] msgLength = new char[4];
        reader.read(msgLength);
        Integer length = Integer.valueOf(new String(msgLength));
        System.out.println("Message length : "+length);

        // 4. Read data according to message length
        char[] msgData = new char[length];
        reader.read(msgData);
        String data = new String(msgData);
        System.out.println("Message data : "+data);

        // 5. Parse MTI
        Integer position = 0;
        Integer MTI_LENGTH = 4;
        String mti = data.substring(position, position + MTI_LENGTH);
        position = position + MTI_LENGTH;
        System.out.println("MTI : "+mti);

        // 6. Parse Bitmap

        // 6.a Get Primary Bitmap
        Integer BITMAP_LENGTH = 16;
        String strPrimary = data.substring(position, position + BITMAP_LENGTH);
        position = position + BITMAP_LENGTH;
        System.out.println("Primary Bitmap (Hex) : " + strPrimary);
        BigInteger primaryBitmap = new BigInteger(strPrimary, 16);
        System.out.println("Primary Bitmap (Bin) : "+String.format("%1$16s",primaryBitmap.toString(2))
                .replace(' ', '0'));

        // 6.b Check whether secondary bitmap exists
        BigInteger secondaryBitmap = null;
        if (primaryBitmap.testBit(64 - 1)) {
            String strSecondary = data.substring(position, position + BITMAP_LENGTH);
            position = position + BITMAP_LENGTH;
            System.out.println("Secondary Bitmap (Hex) : " + strSecondary);

            secondaryBitmap = new BigInteger(strSecondary, 16);
            System.out.println("Secondary Bitmap (Bin) : " + secondaryBitmap.toString(2));
        }

        Map<Integer, String> isomsgData = new LinkedHashMap<>();

        // 7. Extract data at each slot (Data Element)
        Integer startDataElement = 2;
        for (int i = startDataElement; i < 65; i++) {
            if (primaryBitmap.testBit(64 - i)) {
                position = displayDataElement(data, position, i, isomsgData);
                System.out.println("Position : "+position);
            }
        }

        System.out.println("Position after primary bitmap data : " +position);
        System.out.println("Start data element : " +startDataElement);


        if (secondaryBitmap != null) {
            for (int i = startDataElement; i < 129; i++) {
                if (secondaryBitmap.testBit(128 - i)) {
                    position = displayDataElement(data, position, i, isomsgData);
                }
            }
        }

        // 8. Hit core bank REST API for balance inquiry
        CoreBankRestClient bankRestClient = new CoreBankRestClient();
        BigDecimal balance = bankRestClient.inquireBalance(isomsgData.get(2).substring(2), isomsgData.get(52));
        System.out.println("Balance : "+balance);

        // 9. Create ISOMessage Response
        Map<Integer, String> isoresponse = new HashMap<>(isomsgData);
        isoresponse.put(4, ISO8583Helper.padLeft(balance.setScale(0, BigDecimal.ROUND_HALF_EVEN).toString(), '0', 12));
        isoresponse.put(39, "00");

        String strIsoresponse = ISO8583Helper.formatMessageForSending("0110", isoresponse);
        System.out.println("ISO Response : " + strIsoresponse);

        // 10. Create outputstream to send data
        PrintWriter output = new PrintWriter(socket.getOutputStream());
        output.print(strIsoresponse);
        output.flush();
        socket.close();
    }

    private static Integer displayDataElement(String data, Integer position, int i, Map<Integer, String> isomsgData) {
        Integer dataElementLength = MessageConstants.DATA_ELEMENT_LENGTH.get(i);
        //System.out.println("DE "+i+" Length : "+dataElementLength);
        String dataContent = data.substring(position, position + dataElementLength);
        System.out.println("Data element "+ i +" : "+dataContent);
        isomsgData.put(i, dataContent);
        return position + dataElementLength;
    }
}
