package com.muhardin.training.iso8583.server;

import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.util.Map;

public class CoreBankRestClient {

    private static final String RESPONSE_CODE_SUCCESS = "00";

    private String serverBaseUrl = "http://localhost:8080";
    private String inquiryBalanceUrl = "/card/{number}/balance";

    private WebClient webClient;

    public CoreBankRestClient() {
        webClient = WebClient.create(serverBaseUrl);
    }

    public BigDecimal inquireBalance(String cardNumber, String pin) {
        Map<String, Object> result =
                webClient.get()
                        .uri(uriBuilder ->
                                uriBuilder.queryParam("pin", pin)
                        .path("/card/{number}/balance")
                        .build(cardNumber))
                        .accept(MediaType.APPLICATION_JSON)
                        .exchange()
                        .block().bodyToMono(Map.class).block();

        if (RESPONSE_CODE_SUCCESS.equals(result.get("response_code"))) {
            return new BigDecimal((Double) result.get("amount"));
        }

        return BigDecimal.ZERO;
    }
}
