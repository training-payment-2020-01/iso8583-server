package com.muhardin.training.iso8583.server;

import java.math.BigDecimal;

public class CoreBankRestClientTest {
    public static void main(String[] args) {
        CoreBankRestClient client = new CoreBankRestClient();
        BigDecimal balance
                = client.inquireBalance("1234567800001111", "1234");
        System.out.println("Balance = "+balance);
    }
}
